'''
Программа сравнения различных вариантов сортировки целых чисел.
Программа разработана при непосредственном участии Сергея  и Руслана
 
'''

# Импорт пакетов
import re
import time
from tkinter import *
from tkinter import ttk
import tkinter.messagebox as box
import sqlite3
import pandas as pd

class DB:

    '''
    Работа с Базой данный Sqlite
    
    '''
    
    def __init__(self):
        global df
        # открыть базу
        self.opendb = sqlite3.connect("baza_t.db")
        # создать курсор управления
        self.cursup = self.opendb.cursor()
        # если нет таблиц создаем их
        self.cursup.execute(
            "CREATE TABLE IF NOT EXISTS buy (id INTEGER PRIMARY KEY, sort TEXT, date TEXT, num TEXT, time TEXT)")
        # сохраняем базу и создаем таблицы в Pandas
        df = pd.read_sql("SELECT * FROM buy", con=self.opendb)
        self.opendb.commit()
    def __del__(self):
        # закрыть базу
        self.opendb.close()
    def vidall(self):
        # просмотр базы
        self.cursup.execute("SELECT * FROM buy")
        rows = self.cursup.fetchall()
        return rows
    def add_time(self, sort, date, num, time):
        # добавить запись в базу
        self.cursup.execute("INSERT INTO buy VALUES (NULL, ?, ?, ?, ?)", (sort, date, num, time,))
        self.opendb.commit()


def click_open_baza():
    # Создаем базу
    global db, df
    db = DB()
    list1.delete(0, END)
    for row in db.vidall():
        list1.insert(END, row)

def click_add():
    # Добавляем запись в базу
    if not find_error:
        click_open_baza()
        sort  = select_find
        date = date_now
        num = len (int_data)
        time = str(time_sort)
        db.add_time(sort, date, num, time)
        click_open_baza()

'''
Набор функий сорировки

'''


def sort_standart(int_data):
    '''
    Сортировка при помощи стандартной команды Python - 'sort'


    Parameters
    ----------
    int_data : list
        DESCRIPTION.

    Returns
    -------
    sort_data : list
        Отсортированный список.
    time_sort : float
        Время затраченное на сортировку, в мкс.
    '''
 
    start_time = time.time()
    sort_data = sorted (int_data)
    time.sleep(1)
    finish_time = time.time()
    time_sort  = finish_time - start_time
    return (sort_data, time_sort)


def sort_buble(int_data):
    '''
    Сортировка методом пузырька


    Parameters
    ----------
    int_data : list
        DESCRIPTION.

    Returns
    -------
    sort_data : list
        Отсортированный список.
    time_sort : float
        Время затраченное на сортировку, в мкс.

    '''
    
    start_time = time.time()
    last_elem_index = len(int_data) - 1
    for passNo in range (last_elem_index, 0, -1):
        for idx in range (passNo):
            if int_data[idx] > int_data[idx + 1]:
                int_data[idx], int_data[idx+1] = int_data[idx+1], int_data[idx]
    sort_data = int_data
    time.sleep(1)
    finish_time = time.time()
    time_sort  = finish_time - start_time
    return (sort_data, time_sort)
 
    
def sort_count(int_data):
    '''
   
    Сортировка методом подчета


    Parameters
    ----------
    int_data : list
        DESCRIPTION.

    Returns
    -------
    sort_data : list
        Отсортированный список.
    time_sort : float
        Время затраченное на сортировку, в мкс.

    '''
    
    start_time = time.time()
    i_min, i_max = min(int_data), max(int_data)
    lower_bound = i_min
    if i_min < 0:
        lb = abs(i_min)
        int_data = [item + lb for item in nums]
        lower_bound , i_max = min(int_data), max(int_data)
    
    counter_nums = [0]*(i_max-lower_bound+1)
    for item in int_data:
        counter_nums[item-lower_bound] += 1
    pos = 0
    for idx, item in enumerate(counter_nums):
        num = idx + lower_bound
        for i in range(item):
            int_data[pos] = num
            pos += 1
    if i_min < 0:
        lb = abs(i_min)
        int_data = [item - lb for item in nums]
    time.sleep(1)
    finish_time = time.time()
    time_sort  = finish_time - start_time
    return (int_data)
    
    

def heapify(int_data, n, i):
    '''
    Сортировка перамидальным методом 
    Фунция предобразования списка в двоичную кучу

    Parameters
    ----------
    int_data : list
        DESCRIPTION.

    '''    
    largest = i 
    l = 2 * i + 1 
    r = 2 * i + 2 
    if l < n and int_data[i] < int_data[l]:
        largest = l
    if r < n and int_data[largest] < int_data[r]:
        largest = r
    if largest != i:
        int_data[i],int_data[largest] = int_data[largest],int_data[i] # свап
        heapify(int_data, n, largest)

def sort_heap(int_data):
    
    '''
    Сортировка перамидальным методом 
 
    Parameters
    ----------
    int_data : list
        DESCRIPTION.

    Returns
    -------
    sort_data : list
        Отсортированный список.
    time_sort : float
        Время затраченное на сортировку, в мкс.
        
    '''
    
    start_time = time.time()
    n = len(int_data)
    for i in range(n, -1, -1):
        heapify(int_data, n, i)
    for i in range(n-1, 0, -1):
        int_data[i], int_data[0] = int_data[0], int_data[i] # свап 
        heapify(int_data, i, 0)
    time.sleep(1)
    finish_time = time.time()
    time_sort  = finish_time - start_time
    return (int_data, time_sort)



def sort_merge(int_data):
    '''
    
    Сортировка методом слияния 

    Parameters
    ----------
    int_data : list
        DESCRIPTION.

    Returns
    -------
    result : list
        Отсортированный список.
    time_sort : float
        Время затраченное на сортировку, в мкс.

    '''
    start_time = time.time()
    if len(int_data)==1:
        return int_data
    mid = (len(int_data)-1) // 2
    lst1 = sort_merge(int_data[:mid+1])
    lst2 = sort_merge(int_data[mid+1:])
    result = merge(lst1, lst2)
    time.sleep(1)
    finish_time = time.time()
    time_sort  = finish_time - start_time
    return result

def merge(lst1, lst2):
    '''
    
    Сортировка методом слияния 
    Функция объединения списков
    Parameters
    ----------
    int_data : list
        DESCRIPTION.

    Returns
    -------
    result : list
        Отсортированный список.
    time_sort : float
       Время затраченное на сортировку, в мкс.

    '''
    lst = []
    i = 0
    j = 0
    while(i<=len(lst1)-1 and j<=len(lst2)-1):
        if lst1[i]<lst2[j]:
            lst.append(lst1[i])
            i+=1
        else:
            lst.append(lst2[j])
            j+=1
    if i>len(lst1)-1:
        while(j<=len(lst2)-1):
            lst.append(lst2[j])
            j+=1
    else:
        while(i<=len(lst1)-1):
            lst.append(lst1[i])
            i+=1
    return lst



def sort_quick(int_data):
    '''
    
    Сортировка быстрая

    Parameters
    ----------
    int_data : list
        DESCRIPTION.

    Returns
    -------
    int_data : list
        Отсортированный список.
    time_sort : float
        Время затраченное на сортировку, в мкс.


    '''
    start_time = time.time()
    if len(int_data)> 1:
        pivot=int_data.pop()
        grtr_lst, equal_lst, smlr_lst = [], [pivot], []
        for item in int_data:
            if item == pivot:
                equal_lst.append(item)
            elif item > pivot:
                grtr_lst.append(item)
            else:
                smlr_lst.append(item)
        time.sleep(1)
        finish_time = time.time()
        time_sort  = finish_time - start_time
        return (sort_quick(smlr_lst) + equal_lst + sort_quick(grtr_lst))
    else:
        return int_data


def sort_radix(int_data):
    '''
    Сортировка поразрядная 

    Parameters
    ----------
    int_data : list
        DESCRIPTION.

    Returns
    -------
    result : list
        Отсортированный список.
    time_sort : float
        Время затраченное на сортировку, в мкс.

    '''
    start_time = time.time()
    max_digit = max ([len(str(num)) for num in int_data])
    radix = 10
    lists = [[] for i in range (radix)]
    for i in range(0, max_digit):
        for elem in int_data:
            digit = (elem // radix ** i) % radix
            lists[digit].append(elem)
        int_data = [x for queue in lists for x in queue]
        lists = [[] for i in range (radix)]
    time.sleep(1)
    finish_time = time.time()
    time_sort  = finish_time - start_time    
    return (int_data, time_sort)




window = Tk()
window.title("Итоговая аттестация Тищенко Ю.И.")
window.geometry("810x300") #Задаем размеры окна

best_time = 10000
select_find = 'Стандартная  '
date_now = time.ctime()

frame = Frame(window)#Создаем фрейм, в который будет размещено поле для ввода
entry = Entry(frame) #Создаем поле ввода и привязываем его к фрейму


def dialog():
    '''
    Фунция обработки нажатия кнопки

    '''
    global best_time # переменная, хранящее лучшее время в этой сессии программы
    global time_sort # переменная, хранящее лучшее время в этой сессии программы
    global int_data # список данных
    global data_now # переменная, хранящее дату и время сортировки
    global find_error # переменная контролья ошибок
    
    find_error = False # переменная ошибки ввода
    data = entry.get() # приничаем данные из поля ввода
    re_data = re.findall('\d+', data) # выбираем все числа
    find_err = re.findall('\D+', data) # выбираем все не числа
    
    for i in find_err:
        if i != ',':
            find_error = True
            box.showinfo("ВНИМАНИЕ!!!", 'Будьте внимательнее при вводе даннных! Добжны быть только числа разделенные запятыми!')
            
    int_data = [int(item) for item in re_data] # перевод списка строк в список integer
    if len (int_data) == 0: find_error = True # проверка на пустое поле ввода
    
    # обработка выбора метода сортировки
    if not(find_error):
        match select_find:
            case 'Стандартная  ':
                res = sort_standart(int_data)[0]
            case 'Пузырьком    ':
                res = sort_buble(int_data)[0]
            case 'Подсчетом    ':
                res = sort_count(int_data)
            case 'Пирамидальная':
                res = sort_heap(int_data)[0]
            case 'Слиянием     ':
                res = sort_merge(int_data)
            case 'Быстрая      ':
                res = sort_quick(int_data)
            case 'Поразрядная  ':
                res = sort_radix(int_data)[0]
                
        label.configure(text = res)
        time_sort = 1000000*(sort_standart(int_data)[1])-999950 # перевод времени в микросекунды
        # вывод окошка с временем сортировки
        if time_sort > best_time:
            box.showinfo("Время выполнения", str(time_sort)+' мкс.')
        else:
            box.showinfo("THE BEST!!! Время выполнения", str(time_sort) + ' мс.')
            best_time = time_sort

def selected(event):
    '''
    Фунция обработки выбора метода сортировки из выпадающего меню 

    '''
    global select_find
    select_find = combobox.get()     # получаем выделенный элемент
    return (select_find)
 
menu = Menu(window)
btn = Button(frame, text = "Ввод", command = dialog) #Создаем кнопку
lb1 = Label(frame, text = "Введите данные для сортировки через запятую: ") #Создаем метку с поясняющим текстом
lb2 = Label(frame, text = "     Выберете метод сортировки: ") #Создаем метку с поясняющим текстом
label = Label(window, text = " ", font = "Arial 14") #Поле вывода отсортированного списка

list1 = Listbox(window, height=30, width=100, font="Arial 10 normal roman")


# Меню
files1 = Menu(menu, tearoff = False)
files1.add_command(label='Открыть/Создать', command=click_open_baza)
files1.add_command(label='Сохранить', command=click_add)  
files1.add_command(label='Выход', command=window.destroy)
menu.add_cascade(label='Файл', menu=files1)
window.config(menu=menu)

#создание раскрывающегося списка
sort = ['Стандартная  ','Пузырьком    ','Подсчетом    ', 'Пирамидальная', 'Слиянием     ', 'Быстрая      ', 'Поразрядная  ']
combobox = ttk.Combobox(frame, textvariable = sort[0], values=sort)
combobox.set(sort[0])
combobox.bind('<<ComboboxSelected>>', selected)

#Добавляем все виджеты на фрейм
lb1.pack(side = LEFT)
entry.pack(side = LEFT)
lb2.pack(side = LEFT)
combobox.pack(side = LEFT) 
btn.pack(side = RIGHT, padx = 5)
frame.pack(padx = 20, pady = 20)
label.pack(expand=1, fill=X)
list1.pack(padx = 0, pady = 20)

window.mainloop()

