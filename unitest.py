import unittest
import finish_attest 

class Test(unittest.TestCase):


    def test_standart(self):
        self.assertEqual(finish_attest.sort_standart([3,2,1])[0], [1,2,3])
        
    def test_buble(self):
        self.assertEqual(finish_attest.sort_buble([3,2,1])[0], [1,2,3])
            
    def test_count(self):
        self.assertEqual(finish_attest.sort_count([3,2,1]), [1,2,3])
        
    def test_heap(self):
        self.assertEqual(finish_attest.sort_heap([3,2,1])[0], [1,2,3])
        
    def test_merge(self):
        self.assertEqual(finish_attest.sort_merge([3,2,1]), [1,2,3])
        
    def test_quick(self):
        self.assertEqual(finish_attest.sort_quick([3,2,1]), [1,2,3])
        
    def test_radix(self):
        self.assertEqual(finish_attest.sort_radix([3,2,1])[0], [1,2,3])

if __name__ == "__main__":
  unittest.main()